
// 首页
import index from '@/views/index';

const router = [{
  path: '/index',
  name: 'user首页',
  component: index,
  iconCls: 'el-icon-tickets',
  children: [{
    path: '/user/commit',
    name: '商品评价',
    component: () => import('@/views/users/commit'),
    hidden: true,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/user/goods',
    name: '商品选购',
    component: () => import('@/views/users/goods'),
    hidden: true,
    meta: {
      requireAuth: true
    }
  }]
}]


export default router