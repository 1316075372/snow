
// 首页
import index from '@/views/index';

const router = [{
  path: '/index',
  name: 'shopper首页',
  component: index,
  iconCls: 'el-icon-tickets',
  children: [
    {
      path: '/shopper/menu',
      name: '菜单管理',
      component: () => import('@/views/shopper/menu'),
      hidden: true,
      meta: {
        requireAuth: true
      }
    },
    {
      path: '/shopper/sells',
      name: '销售管理',
      component: () => import('@/views/shopper/sell'),
      hidden: true,
      meta: {
        requireAuth: true
      }
    },
    {
      path: '/common/users',
      name: '个人中心',
      component: () => import('@/views/shopper/user'),
      meta: {
        requireAuth: true
      }
    },]
}]


export default router