// 导入组件
import Vue from 'vue';
import Router from 'vue-router';
import routerAdmin from './router_admin'
import routerCommon from './router_common'
import routerUser from './router_user'
import routerShopper from './router_shopper'

// 启用路由
Vue.use(Router);
let routes = new Set([...routerAdmin, ...routerCommon, ...routerUser, ...routerShopper]);

const router = new Router({
  routes,
  mode: 'history'
})

// 导出路由 
export default router