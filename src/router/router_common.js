// 登录
import login from '@/views/login/login';
// 首页
import index from '@/views/index';
// 注册
import register from '@/views/register/register'

const routerDemo = [{
  path: '/',
  name: '',
  component: login,
  hidden: true,
  meta: {
    requireAuth: false
  }
}, {
  path: '/login',
  name: '登录',
  component: login,
  hidden: true,
  meta: {
    requireAuth: false
  }
}, {
  path: '/register',
  name: '注册',
  component: register,
  hidden: true,
  meta: {
    requireAuth: false
  }
}, {
  path: '/index',
  name: '首页',
  component: index,
  iconCls: 'el-icon-tickets',
  children: [{
    path: '/goods/Goods',
    name: '商品管理',
    component: () => import('@/views/goods/Goods'),
    meta: {
      requireAuth: true
    }
  },
  // 异常
  {
    path: '/abnormal/404',
    name: 'error-404',
    meta: {
      title: '404'
    },
    component: (resolve) => require(['@/views/abnormal/404/404'], resolve)
  },
  {
    path: '/abnormal/500',
    name: 'error-500',
    meta: {
      title: '500'
    },
    component: (resolve) => require(['@/views/abnormal/500/500'], resolve)
  },
  {
    path: '/abnormal/403',
    name: 'error-403',
    meta: {
      title: '拒绝访问'
    },
    component: (resolve) => require(['@/views/abnormal/403/403'], resolve)
  },
  {
    path: '/abnormal/unknown',
    name: 'unknown',
    meta: {
      title: '未知错误'
    },
    component: (resolve) => require(['@/views/abnormal/unknown/unknown'], resolve)
  }]
}]
export default routerDemo