
// 首页
import index from '@/views/index';

const router = [{
  path: '/index',
  name: 'admin首页',
  component: index,
  iconCls: 'el-icon-tickets',
  children: [{
    path: '/admin/approval',
    name: '商家信息审批',
    component: () => import('@/views/admin/MerchantManagement/infoApproval'),
    hidden: true,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/admin/businessInfo',
    name: '商家信息管理',
    component: () => import('@/views/admin/MerchantManagement/businessInfo'),
    hidden: true,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/admin/boothInfo',
    name: '摊位信息',
    component: () => import('@/views/admin/BoothManagement/boothInfo'),
    hidden: true,
    meta: {
      requireAuth: true
    }
  }]
}]


export default router