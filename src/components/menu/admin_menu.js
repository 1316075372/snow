const res = {
  data: [
    {
      menuid: 11,
      icon: 'icon-news-manage',
      menuname: '商家管理',
      hasThird: null,
      url: null,
      menus: [
        {
          menuid: 12,
          icon: 'icon-cat-skuQuery',
          menuname: '商家信息审批',
          hasThird: 'N',
          url: 'admin/approval',
          menus: null
        },
        {
          menuid: 13,
          icon: 'icon-cat-skuQuery',
          menuname: '商家信息管理',
          hasThird: 'N',
          url: 'admin/businessInfo',
          menus: null
        }
      ]
    },
    {
      menuid: 14,
      icon: 'li-icon-dingdanguanli',
      menuname: '摊位管理',
      hasThird: null,
      url: 'admin/boothInfo',
      menus: [
        {
          menuid: 15,
          icon: 'icon-order-manage',
          menuname: '摊位信息',
          hasThird: 'N',
          url: 'admin/boothInfo',
          menus: null
        }
      ]
    },
  ]
}
export default res