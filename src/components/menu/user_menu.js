const res = {
  data: [
    {
      menuid: 1,
      icon: 'li-icon-xiangmuguanli',
      menuname: '基础管理',
      hasThird: null,
      url: null,
      menus: [
        {
          menuid: 2,
          icon: 'icon-cat-skuQuery',
          menuname: '商品选购',
          hasThird: 'N',
          url: 'user/goods',
          menus: null
        },
        // {
        //   menuid: 3,
        //   icon: 'icon-cat-skuQuery',
        //   menuname: '商品预定',
        //   hasThird: 'N',
        //   url: 'user/Order',
        //   menus: null
        // },
        {
          menuid: 4,
          icon: 'icon-cat-skuQuery',
          menuname: '商品打分',
          hasThird: 'N',
          url: 'user/commit',
          menus: null
        }
      ]
    }
  ]
}
export default res