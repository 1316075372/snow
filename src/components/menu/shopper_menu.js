const res = {
  data: [
    {
      menuid: 21,
      icon: 'li-icon-xiangmuguanli',
      menuname: '系统管理',
      hasThird: null,
      url: null,
      menus: [
        {
          menuid: 22,
          icon: 'icon-order-manage',
          menuname: '菜单管理',
          hasThird: 'N',
          url: 'shopper/menu',
          menus: null
        },
        {
          menuid: 23,
          icon: 'icon-order-manage',
          menuname: '销售管理',
          hasThird: 'N',
          url: 'shopper/sells',
          menus: null
        },
        {
          menuid: 24,
          icon: 'icon-order-manage',
          menuname: '个人中心',
          hasThird: 'N',
          url: 'common/users',
          menus: null
        }
      ]
    }
  ]
}
export default res