import http from '../../../api/axios'

export default {
  getCommit(page, size, data) {
    var url = data ? '/api/api/sorder/search/' + page + '/' + size + '?' + data : '/api/api/sorder/search/' + page + '/' + size
    return http.post(url)
  },
  getCommitInfo(page, size, data) {
    var url = data ? '/api/api/commodityorder/search/' + page + '/' + size + '?' + data : '/api/api/commodityorder/search/' + page + '/' + size
    return http.post(url)
  },
  saveCommit(data) {
    return http.put('/api/api/commodityorder', data)
  },
  getShopperList(page, size, data) {
    var url = data ? '/api/api/shopper/search/' + page + '/' + size + '?' + data : '/api/api/shopper/search/' + page + '/' + size
    return http.post(url)
  },
  // 获取所有菜单
  getMenu(page, size, data) {
    var url = data ? '/api/api/commoditytype/search/' + page + '/' + size + '?' + data : '/api/api/commoditytype/search/' + page + '/' + size
    return http.post(url)
  },
  // 根据分类获取所有商品
  getFood(id, data) {
    return http.get('/api/scene/sub02/commodityFilterByType/' + id, data)
  },
  saveOrder(data) {
    return http.post('/api/scene/sub03/commitSorder', data)
  },
}