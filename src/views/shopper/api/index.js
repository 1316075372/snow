import http from '../../../api/axios'

export default {
  getShopperInfo(id) {
    return http.get('/api/scene/sub02/getUserAndShopperInfo/' + id)
  },
  updateShopperInfo(data) {
    return http.post('/api/scene/sub02/changeUserAndShopperInfo', data)
  },
  // 获取所有菜单
  getMenu(page, size, data) {
    var url = data ? '/api/api/commoditytype/search/' + page + '/' + size + '?' + data : '/api/api/commoditytype/search/' + page + '/' + size
    return http.post(url)
  },
  addMenu(data) {
    return http.post('/api/api/commoditytype', data)
  },
  deleteMenu(id) {
    return http.delete('/api/api/commoditytype/' + id)
  },
  updateMenu(data) {
    return http.put('/api/api/commoditytype', data)
  },
  // 根据分类获取所有商品
  getFood(id, data) {
    return http.get('/api/scene/sub02/commodityFilterByType/'+ id ,data)
  },
  addFood(data) {
    return http.post('/api/api/commodity', data)
  },
  deleteFood(id) {
    return http.delete('/api/api/commodity/' + id)
  },
  updateFood(data) {
    return http.put('/api/api/commodity', data)
  },
  // 获取所有订单
  getOrder(page, size, data) {
    var url = data ? '/api/api/sorder/search/' + page + '/' + size + '?' + data : '/api/api/sorder/search/' + page + '/' + size
    return http.post(url)
  },
  // 获取销售信息
  getSalesInfo(id, data) {
    return http.get('/api/scene/sub02/getCommodityOrderByShopper/' + id, data)
  }
}