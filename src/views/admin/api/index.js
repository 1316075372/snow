import http from '../../../api/axios'

export default {
  getShopperList(page, size, data) {
    var url = data ? '/api/api/shopper/search/' + page + '/' + size + '?' + data : '/api/api/shopper/search/' + page + '/' + size
    return http.post(url)
  },
  updateState(data) {
    return http.put('/api/api/shopper', data)
  },
  getShopperInfo(id) {
    return http.get('/api/scene/sub02/getUserAndShopperInfo/' + id)
  },
  updateShopperInfo(data) {
    return http.post('/api/scene/sub02/changeUserAndShopperInfo', data)
  },
  updateMoney(data) {
    return http.get('/api/scene/sub04/setShopperPrice?', data)
  }
}